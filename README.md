# Задание
Дан пустой аккаунт в AWS. Необходимо развернуть инфраструктуру и процесс доставки приложения, состоящую из:

- виртуальной сети, VPC
- виртуальных серверов, где будет работать приложение и сопутствующие сервисы, EC2
- балансировщика трафика, который будет предоставлять доступ к приложению внешним пользователям ELB
- gitlab ci пайплайна, с помощью которого будет билдиться и доставляться приложение на виртуальный сервер
- мониторинга виртуального сервера (минимум необходимо сделать мониторинг CPU, RAM, Disk Usage)

## Список средств, которые необходимо использовать:
- terraform
- ansible
- gitlab ci
- prometheus, node_exporter, aws cloudwatch
- grafana, aws cloudwatch

Можно взять аналоги/замены, объяснив почему именно они использовались.

Приложение предлагается выбрать самому, либо написать минимальное приложение на python (fastapi), go или java, которое будет отдавать JSON `{"message": "hello world"}` на запрос `GET /`, при необходимости можно сделать необходимые дополнительные пути в приложении.
VPC и подсети разрешается создать в интерфейсе AWS в обход IaC.

Типы виртуальных серверов надо выбрать минимальный из необходимого, предпочтение отдать типу t3.micro/small/medium.  Автоскалирование делать не нужно.

В мониторинге достаточно сделать дашборды с данными мониторинга, алерты не нужны.
Приложение и код инфраструктуры можно положить в личную репу на [gitlab.com](http://gitlab.com/) и там же запускать gitlab ci на публичных gitlab runner-ах.

### How to run

```bash
pip install -r requirements.txt
uvicorn main:app --reload
```
### Требования к ПО 

`Python 3.10`